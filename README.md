# filebrowser-read-only

An image for [File Browser](https://github.com/filebrowser/filebrowser) but it's read-only. Authentication is disabled and files can only be downloaded. The file browser displays the contents of the `/srv/` directory.

The image is tagged as `corysanin/filebrowser-read-only` and `registry.gitlab.com/sanin.dev/filebrowser-read-only`.

NOTE: The contents of this repository are licensed under [MIT](/LICENSE) but File Browser is licensed under [Apache-2.0](https://github.com/filebrowser/filebrowser/blob/master/LICENSE).