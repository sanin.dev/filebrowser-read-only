FROM filebrowser/filebrowser:latest as deploy

RUN /filebrowser config init --address=0.0.0.0 --auth.method=noauth --branding.disableUsedPercentage=true --branding.disableExternal=true && \
    /filebrowser users add admin admin \
    --perm.admin=false --perm.create=false --perm.delete=false --perm.execute=false --perm.modify=false --perm.rename=false --perm.share 

ENTRYPOINT [ "/filebrowser" ]
